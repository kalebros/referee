/****************************************************************************/
///
/// This file is part of Referee.
/// @2016, Antonio Ramírez Marti (morgulero@gmail.com)
//
///    Referee is free software: you can redistribute it and/or modify
///    it under the terms of the GNU General Public License as published by
///    the Free Software Foundation, either version 3 of the License, or
///    (at your option) any later version.
///
///    Referee is distributed in the hope that it will be useful,
///    but WITHOUT ANY WARRANTY; without even the implied warranty of
///    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///    GNU General Public License for more details.
///
///    You should have received a copy of the GNU General Public License
///    along with Foobar.  If not, see <http://www.gnu.org/licenses/>
///
/****************************************************************************/

import QtQuick 2.6
import Qt.labs.templates 1.0 as T

//Button {
//    property string imagen: "qrc:/images/hexagon.png"
//    property string buttonColor: "steelblue"
//    id: torneoButton
//    background: Rectangle {
//        color: torneoButton.pressed ? "lightsteelblue" : "transparent"
//        border.color: torneoButton.pressed ? "lightsteelblue" : buttonColor
//        radius: 5
//        Image {
//            id: imSource
//            height: torneoButton.availableHeight-20
//            width: torneoButton.availableWidth
//            source: imagen
//            x: torneoButton.leftPadding
//            y: torneoButton.topPadding
//        }
//    }
//    label: Label {
//       x: torneoButton.leftPadding
//       y: torneoButton.topPadding
//       text: torneoButton.text
//       horizontalAlignment: Text.AlignHCenter
//       verticalAlignment: Text.AlignBottom
//       height: torneoButton.availableHeight
//       width: torneoButton.availableWidth
//       color: torneoButton.pressed ? "white" : buttonColor
//    }

//}

T.Button {
    id: tButton
    property color buttonColor: "steelblue"
    property string imagen: "qrc:/images/hexagon.png"
    background: Rectangle {
        radius: 5
        border.color: buttonColor
        color: tButton.pressed ? buttonColor : "transparent"
        implicitHeight: 40
        implicitWidth: 100
        anchors.fill: parent
    }
    label: Column {
        x: tButton.padding+4
        y: tButton.padding +4
        spacing: 5
        Image {
            id: imBack
            source: imagen
            height: tButton.availableHeight-8-tBText.height-4
            width: tButton.availableWidth-8
        }
        Text {
            id: tBText
            text: tButton.text
            color: tButton.pressed ? "white" : buttonColor
            width: background.width-8
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

}
