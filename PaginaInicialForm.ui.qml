/****************************************************************************/
///
/// This file is part of Referee.
/// @2016, Antonio Ramírez Marti (morgulero@gmail.com)
//
///    Referee is free software: you can redistribute it and/or modify
///    it under the terms of the GNU General Public License as published by
///    the Free Software Foundation, either version 3 of the License, or
///    (at your option) any later version.
///
///    Referee is distributed in the hope that it will be useful,
///    but WITHOUT ANY WARRANTY; without even the implied warranty of
///    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///    GNU General Public License for more details.
///
///    You should have received a copy of the GNU General Public License
///    along with Foobar.  If not, see <http://www.gnu.org/licenses/>
///
/****************************************************************************/

import QtQuick 2.6
import Qt.labs.controls 1.0
import QtQuick.Layouts 1.0

Page {
    id: page1
    width: 640
    height: 400
    property alias catanButton: catanButton
    property alias carcasoneButton: carcasoneButton
    property alias loadButton: refereeButton1

    Rectangle {
        id: rectangle1
        height: 80
        color: "#4682b4"
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        border.color: "#4682b4"

        Label {
            id: label1
            x: 222
            y: 14
            text: qsTr("CCReferee")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: 30
            color: "white"
        }
    }

    RefereeButton {
        id: refereeButton1
        x: 209
        width: 223
        height: 26
        text: "Cargar torneo"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: rectangle1.bottom
        anchors.topMargin: 29
        spacing: 19
        Layout.preferredHeight: 26
        Layout.preferredWidth: 197
    }

    Label {
        id: label2
        x: 209
        width: 223
        height: 14
        text: qsTr("Nuevo torneo")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: refereeButton1.bottom
        anchors.topMargin: 21
        horizontalAlignment: Text.AlignHCenter
        Layout.preferredHeight: 13
        Layout.preferredWidth: 82
    }

    RowLayout {
        x: 181
        width: 280
        height: 128
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: label2.bottom
        anchors.topMargin: 31
        spacing: 100

        TorneoButton {
            id: carcasoneButton
            width: 104
            height: 120
            text: "Carcasonne"
            property string property0: "none.none"
            Layout.preferredHeight: 105
            Layout.preferredWidth: 90
            imagen: "qrc:/images/rectangle.png"
        }

        TorneoButton {
            id: catanButton
            height: 120
            buttonColor: "orange"
            text: "Catan"
            Layout.columnSpan: 1
            Layout.rowSpan: 1
            Layout.preferredHeight: 105
            Layout.preferredWidth: 90
            checkable: false
        }
    }
}
