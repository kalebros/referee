/****************************************************************************/
///
/// This file is part of Referee.
/// @2016, Antonio Ramírez Marti (morgulero@gmail.com)
//
///    Referee is free software: you can redistribute it and/or modify
///    it under the terms of the GNU General Public License as published by
///    the Free Software Foundation, either version 3 of the License, or
///    (at your option) any later version.
///
///    Referee is distributed in the hope that it will be useful,
///    but WITHOUT ANY WARRANTY; without even the implied warranty of
///    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///    GNU General Public License for more details.
///
///    You should have received a copy of the GNU General Public License
///    along with Foobar.  If not, see <http://www.gnu.org/licenses/>
///
/****************************************************************************/

import QtQuick 2.6
import QtQuick.Layouts 1.0
import Qt.labs.controls 1.0

ApplicationWindow {
    id: applicationWindow1
    visible: true
    width: 640
    height: 960
    title: qsTr("CCReferee")

//    header: Rectangle {
//        implicitHeight: 100
//        implicitWidth: 640
//        color: "steelblue"
//        Label {
//            anchors.centerIn: parent
//            text: "Referee"
//            color: "white"
//            font.pointSize: 30
//        }
//    }

    PaginaInicial {
        id: inicio
        width: stackView.availableWidth
        height: stackView.availableHeight
    }

    PaginaTorneo {
        id: torneoCatan
        width: stackView.availableWidth
        height: stackView.availableHeight
        title: "Catan"
        titleColor: "orange"
        fieldColor: "orange"
    }

    PaginaTorneo {
        id: torneoCarcasonne
        width: stackView.availableWidth
        height: stackView.availableHeight
        title: "Carcasonne"
        titleColor: "steelBlue"
        roundColor: "steelBlue"
        fieldColor: "steelBlue"
    }

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: inicio

        pushEnter: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 0
                    to:1
                    duration: 150
                }
            }
            pushExit: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 1
                    to:0
                    duration: 150
                }
            }
            popEnter: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 0
                    to:1
                    duration: 150
                }
            }
            popExit: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 1
                    to:0
                    duration: 200
                }
            }
    }


//    SwipeView {
//        id: swipeView
//        anchors.fill: parent
//        currentIndex: tabBar.currentIndex

//        PaginaInicial {
//            title: "Inicio"
//            titleColor: "blue"
//            roundColor: "blue"

//        }

//        PaginaInicial {
//            title: "Inicio"
//            titleColor: "orange"
//            roundColor: "orange"
//        }


//    }

    footer: TabBar {
        id: tabBar
        visible: false
        currentIndex: swipeView.currentIndex
        TabButton {
            id: bCarcassone
            text: qsTr("Carcassone")
            background: Rectangle {
                implicitWidth: 200
                implicitHeight: 40
                color: bCarcassone.checked ? "white" : "blue"
            }
        }
        TabButton {
            id: bCatan
            text: qsTr("Catan")
            background: Rectangle {
                implicitWidth: 200
                implicitHeight: 40
                color: bCatan.checked ? "white" : "orange"
            }

        }
    }

//    RefereeButton {
//        id: loadButton
//        x: 227
//        y: 271
//        width: 182
//        height: 26
//        text: "Cargar torneo"
//        anchors.verticalCenter: parent.verticalCenter
//        anchors.horizontalCenter: parent.horizontalCenter
//    }

    Connections {
        target: inicio.catanButton
        onClicked: {
            stackView.pop(inicio);
            stackView.push(torneoCatan);
        }
    }

    Connections {
        target: torneoCarcasonne.cancelButton
        onClicked: {
            stackView.pop(torneoCarcasonne);
            stackView.push(inicio);
        }
    }

    Connections {
        target: torneoCatan.cancelButton
        onClicked: {
            stackView.pop(torneoCatan);
            stackView.push(inicio);
        }
    }

    Connections {
        target: inicio.carcasoneButton
        onClicked: {
            stackView.pop(inicio);
            stackView.push(torneoCarcasonne);
        }
    }
}
