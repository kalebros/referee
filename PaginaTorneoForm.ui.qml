/****************************************************************************/
///
/// This file is part of Referee.
/// @2016, Antonio Ramírez Marti (morgulero@gmail.com)
//
///    Referee is free software: you can redistribute it and/or modify
///    it under the terms of the GNU General Public License as published by
///    the Free Software Foundation, either version 3 of the License, or
///    (at your option) any later version.
///
///    Referee is distributed in the hope that it will be useful,
///    but WITHOUT ANY WARRANTY; without even the implied warranty of
///    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///    GNU General Public License for more details.
///
///    You should have received a copy of the GNU General Public License
///    along with Foobar.  If not, see <http://www.gnu.org/licenses/>
///
/****************************************************************************/

import QtQuick 2.6
import Qt.labs.controls 1.0

PageBackground {
    id: pageBackground1
    property alias name: nombreTorneo
    width: 400
    height: 400
    fieldColor: "#f86608"
    property alias cancelButton: cButton

    RefereeButton {
        id: cButton
        x: 166
        y: 366
        text: "Cancelar"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
    }

    CCRTextField {
        id: nombreTorneo
        x: 64
        y: 187
        width: 272
        height: 34
        bottomColor: "#f86608"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }
}
