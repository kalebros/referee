/****************************************************************************/
///
/// This file is part of Referee.
/// @2016, Antonio Ramírez Marti (morgulero@gmail.com)
//
///    Referee is free software: you can redistribute it and/or modify
///    it under the terms of the GNU General Public License as published by
///    the Free Software Foundation, either version 3 of the License, or
///    (at your option) any later version.
///
///    Referee is distributed in the hope that it will be useful,
///    but WITHOUT ANY WARRANTY; without even the implied warranty of
///    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///    GNU General Public License for more details.
///
///    You should have received a copy of the GNU General Public License
///    along with Foobar.  If not, see <http://www.gnu.org/licenses/>
///
/****************************************************************************/

import QtQuick 2.6
import Qt.labs.controls 1.0


Button {
    id: rButton
    background: Rectangle {
        color: rButton.pressed ? "steelblue" : "transparent"
        border.color: "steelblue"
        radius: 5
    }
    label: Label {
        text: rButton.text
        x: rButton.leftPadding
        y: rButton.topPadding
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        height: rButton.availableHeight
        width: rButton.availableWidth
        color: rButton.pressed ? "white" : "steelblue"
    }
}
