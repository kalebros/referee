/****************************************************************************/
///
/// This file is part of Referee.
/// @2016, Antonio Ramírez Marti (morgulero@gmail.com)
//
///    Referee is free software: you can redistribute it and/or modify
///    it under the terms of the GNU General Public License as published by
///    the Free Software Foundation, either version 3 of the License, or
///    (at your option) any later version.
///
///    Referee is distributed in the hope that it will be useful,
///    but WITHOUT ANY WARRANTY; without even the implied warranty of
///    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///    GNU General Public License for more details.
///
///    You should have received a copy of the GNU General Public License
///    along with Foobar.  If not, see <http://www.gnu.org/licenses/>
///
/****************************************************************************/

import QtQuick 2.6

Rectangle {
    id: rectangle1
    property alias roundColor: roundCorner.color
    property alias titleColor: titleText.color
    property alias title: titleText.text
    property color fieldColor: "green"
    width: 640
    height: 960
    border.color: "#ffffff"
    clip: true
    Rectangle {
        id: roundCorner
        x: 478
        y: -16
        width: 252
        height: 118
        color: "#f86608"
        anchors.right: parent.right
        anchors.rightMargin: -95
        clip: true
        rotation: 45
    }

    Text {
        id: titleText
        x: 8
        y: 8
        width: 404
        height: 39
        color: "#f86608"
        text: qsTr("Title")
        font.family: "Arial"
        font.pixelSize: 29
    }

}
